const { Router } = require('express');
const router = Router();

const order = require('../../controllers/user/order');
const contact = require('../../controllers/user/contacts');
const product = require('../../controllers/user/products');
const categories = require('../../controllers/user/categories');
const contactValidator = require('../../validators/user/create-contact');
const orderValidator = require('../../validators/user/create-order');

router.post('/order', orderValidator, order.create);
router.post('/contact', contactValidator, contact.create);
router.get('/categories', categories.all);
router.get('/products/:id', product.all);
router.get('/product/:id', product.find);

module.exports = router