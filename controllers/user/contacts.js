const Contact = require('../../models/conact');
const Email = require('../../services/email');

exports.create = async (req, res) => {

    try {

        const data = { ...req.body }

        const contact = new Contact({
            name: data.name,
            email: data.email,
            phone: data.phone,
            text: data.text,
        });

        const email = new Email({ data:{name: data.name, email: data.email, phone: data.phone, text: data.text}, mail: 'contact' });
        await email.send();

        await contact.save();

        res.json({ 'data': contact, 'status': 'ok' });
    } catch (e) {
        console.log(e)
        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': e });
    }
}