require('dotenv').config();
const secret = process.env.TOKEN_SECRET;
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {

    if (!req.headers.authorization){

        return res.status(401).json({ error: 'No token provided.' });
    }

    const token = req.headers.authorization.split(" ");

    jwt.verify(token[1], secret,(err, data) => {

        if(err){

          return  res.status(401).json({error: 'Token is expired.'});
        }

        req.user = {id : data.id};
        next();
    });
}