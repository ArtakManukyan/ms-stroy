const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const oredrSchema = new Schema({
    name : {
        type : String,
        required: true,
    },
    email : {
        type : String,
        required: true,
    },
    phone : {
        type : String,
        required: true,
    },
    address : {
        type : String,
        required: true,
    },
    products : [
        {
            category: {
                type: Schema.Types.ObjectId,
                ref: "Product",
            },
            count: Number,
            name: String,
            price: String,
            img: String
        }
    ]
});

const Order = mongoose.model('Order', oredrSchema);
module.exports = Order;