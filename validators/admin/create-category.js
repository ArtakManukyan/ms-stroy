const {  validationResult } = require('express-validator');
const { body } = require('express-validator');
const Category = require('../../models/category')
const fs = require('fs');
const path = require('path');

module.exports = [
    body('name')
    .not().isEmpty().withMessage('The name is required.').custom(value => {
        return Category.find({"name": value}).then(category => {

          if (category.length > 0) {
            return Promise.reject('Category already exist.');
          }
        });
      }),

    (req, res, next) => {

        if(!req.file){

            return res.status(400).json({errors: [
                {
                    "value": "",
                    "msg": "The image is required and the file type must be png, jpg or jpeg.",
                    "param": "img",
                    "location": "body"
                }
            ]})
        }

        const errors = validationResult(req);

        if (!errors.isEmpty()) {

            fs.unlink(path.join('images', req.file.filename), (err) => {
                if (err){
                   return res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
                }
            });

            return res.status(400).json({ errors: errors.array() });
        }


        next();
    }
]