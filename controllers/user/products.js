const Product = require('../../models/product')

exports.all = async (req, res) => {

    try {
        const id = req.params.id;

        const products = await Product.find({ "category": id });

        res.json({ 'data': products, 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}


exports.find = async (req, res) => {

    try {
        const id = req.params.id;

        const product = await Product.findById(id);

        res.json({ 'data': product, 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}