require('dotenv').config();
const User = require('../../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const tokenLife = process.env.TOKEN_LIFE;
const secret = process.env.TOKEN_SECRET;
const refreshTokenSecret = process.env.REFRESH_TOKEN_SECRET;
const refreshTokenLife = process.env.REFRESH_TOKEN_LIFE;

exports.login = async (req, res) => {

    try {

        const data = req.body;

        const user = await User.findOne({ email: data.email });
    
        if (!user) {
            return res.status(401).json({ error: 'The email you’ve entered doesn’t match any account.' });
        }
    
        const checkPassword = await bcrypt.compare(data.password, user.password);
    
        if (user && checkPassword) {
    
            const access_token = jwt.sign({ id: user._id }, secret, { expiresIn: parseInt(tokenLife) });
            const refresh_token = jwt.sign({ id: user._id }, refreshTokenSecret, { expiresIn: parseInt(refreshTokenLife) });
    
            res.status(200).json({ refresh_token, access_token });
    
        } else {
    
            res.status(401).json({ error: 'Invalid Email or Password' });
        }

    } catch (e) {
        console.log(e)
        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}

exports.token = (req, res) => {

    try {

        if (!req.headers.refresh) {

            return res.status(401).json({ error: 'No token provided.' });
        }
    
        const token = req.headers.refresh.split(" ");
    
        if (!token || !token[1]) {
    
            return res.status(401).json({ error: 'No token provided.' });
        }
    
        jwt.verify(token[1], refreshTokenSecret, (err, data) => {
    
            if (err) {
    
                return res.json({ error: 'Token is expired.' });
            }
    
            const access_token = jwt.sign({ id: data.id }, secret, { expiresIn: parseInt(tokenLife) });
    
            const refresh_token = jwt.sign({ id: data.id }, refreshTokenSecret, { expiresIn: parseInt(refreshTokenLife) });
    
            res.status(201).json({
                access_token,
                refresh_token
            });
        });
    }  catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }


}