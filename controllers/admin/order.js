const Order = require('../../models/order')

exports.all = async (req, res) => {

    try {

        const orders = await Order.find({});

        res.json({ 'data': orders, 'status': 'ok' });

    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}


exports.show = async (req, res) => {

    try {

        const id = req.params.id;

        const order = await Order.findById(id);

        res.json({ 'data': order, 'status': 'ok' });

    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}