const {  validationResult } = require('express-validator');
const { body } = require('express-validator');
const fs = require('fs');
const path = require('path');

module.exports = [
    body('name')
    .not().isEmpty().withMessage('The name is required.'),
    body('price')
    .not().isEmpty().withMessage('The price is required.'),
    body('category').isString()
    .not().isEmpty().withMessage('The category is required.'),
    body('status').isString(),
    body('brand').isString(),
    body('color').isString(),
    body('country_of_origin').isString(),
    body('size').isString(),
    body('quantity_per_box').isString(),
    body('product_code').isString(),
    body('creator').isString(),
    body('expiration_date').isString(),
    body('description').isString(),


    (req, res, next) => {

        const errors = validationResult(req);

        if(!req.file){

            return res.status(400).json({errors: [
                {
                    "value": "",
                    "msg": "The image is required and the file type must be png, jpg or jpeg.",
                    "param": "img",
                    "location": "body"
                }
            ]})
        }

        if (!errors.isEmpty()) {

            fs.unlink(path.join('images', req.file.filename), (err) => {
                if (err){
                   return res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
                }
            });

            return res.status(400).json({ errors: errors.array() });
        }

        next();
    }
]