const Category = require('../../models/category')
const Product = require('../../models/product')
const fs = require('fs')
const path = require('path')

exports.all = async (req, res) => {

    try {
        const categoris = await Category.find({});

        res.json({ 'data': categoris, 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}

exports.create = async (req, res) => {
    const data = { ...req.body };

    const category = new Category({
        name: data.name,
        img: req.file.filename,
    });

    try {

        await category.save();

        res.json({ 'data': category, 'status': 'ok' });
    } catch (e) {
        console.log(e)
        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}

exports.delete = async (req, res) => {

    try {
        const id = req.params.id;

        const category = await Category.findById(id);

        if (category) {

            fs.unlink(path.join('images', category.img), (err) => {
                if (err) {

                    return res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
                }
            });
        }

        await Product.deleteMany({ "category": id })
        await Category.findByIdAndDelete(id)

        res.json({ 'data': 'Category deleted successfully.', 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}

exports.update = async (req, res) => {

    try {

        const data = { ...req.body };
        const id = req.params.id;

        const category = await Category.findById(id);

        if (!category) {

            return res.status(404).json({ 'error': 'Category not exist.', 'status': 'error' });
        }

        if (req.file) {

            fs.unlink(path.join('images', category.img), (err) => {
                if (err) {
                    return res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
                }
            });

            data.img = req.file.filename;
        } else {

            data.img = category.img;
        }

        await Category.findByIdAndUpdate(id, data);

        res.json({ 'data': 'Category updated successfully.', 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}