const multer = require('multer');

const storage = multer.diskStorage({
    destination(req, file, cd){
        cd(null,'images')
    },
    filename(req, file, cd){
        cd(null, new Date().toISOString().replace(/[\/\\:]/g, "_") + '-' + file.originalname)
    }
})

const allowedTypes = ['image/png', 'image/jpg', 'image/jpeg'];

const fileFilter = (req, file, cd) => {

    if(allowedTypes.includes(file.mimetype)){
        cd(null,true)
    } else{
        cd(null,false)
    }
}

module.exports = multer({
    storage,
    fileFilter
})