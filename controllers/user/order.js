const Order = require('../../models/order');

exports.create = async (req, res) => {

    const data = { ...req.body };

    console.log(data.products)
    const order = new Order({
        name: data.name,
        email: data.email,
        phone: data.phone,
        address: data.address,
        products: data.products,
    });

    try {

        await order.save();

        const total = data.products.reduce((item, current) => (item.count * item.price) + current, 0 );

        // const email = new Email({
        //     data: {
        //         name: data.name,
        //         email: data.email,
        //         phone: data.phone,
        //         address: data.address,
        //         total
        //     },
        //     mail: 'order'
        // });

        // await email.send();

        res.json({ 'data': order, 'status': 'ok' });
    } catch (e) {
        console.log(e)
        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}