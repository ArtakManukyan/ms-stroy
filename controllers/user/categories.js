const Category = require('../../models/category')

exports.all = async (req, res) => {

    try {
        const categoris = await Category.find({});

        res.json({ 'data': categoris, 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}