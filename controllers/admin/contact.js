const Contact = require('../../models/conact')

exports.all = async (req, res) => {

    try {

        const contacts = await Contact.find({});

        res.json({ 'data': contacts, 'status': 'ok' });

    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}


exports.show = async (req, res) => {

    try {

        const id = req.params.id;

        const contact = await Contact.findById(id);

        res.json({ 'data': contact, 'status': 'ok' });

    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}