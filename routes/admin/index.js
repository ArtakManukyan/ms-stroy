const { Router } = require('express');
const router = Router();
const auth = require('../../middlewares/auth');
const upload = require('../../middlewares/upload');

const category = require('../../controllers/admin/category');
const product = require('../../controllers/admin/product');
const order = require('../../controllers/admin/order');
const contact = require('../../controllers/admin/contact');
const authContriller = require('../../controllers/admin/auth');

const productValidator = require('../../validators/admin/create-product');
const categoryValidator = require('../../validators/admin/create-category');
const updateCategoryValidator = require('../../validators/admin/update-category');
const updateProductValidator = require('../../validators/admin/update-product');

router.get('/categoris', auth, category.all);
router.post('/category', auth, upload.single('img'), categoryValidator, category.create);
router.post('/category/:id', auth, upload.single('img'), updateCategoryValidator, category.update);
router.delete('/category/:id', auth, category.delete);

router.get('/products', auth, product.all);
router.post('/product', auth, upload.single('img'), productValidator, product.create);
router.get('/product/:id', auth, product.show);
router.post('/product/:id', auth, upload.single('img'), updateProductValidator, product.update);
router.delete('/product/:id', auth, product.delete);

router.get('/orders', order.all);
router.get('/order/:id', auth, order.show);
// router.post('/order/update/status/:id', category.update);

router.get('/contacts', auth, contact.all);
router.get('/contact/:id', auth, contact.show);


router.post('/login', authContriller.login);
router.post('/token', authContriller.token);

module.exports = router