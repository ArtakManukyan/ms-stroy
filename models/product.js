const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    price: {
        type: String,
        required: true,
    },
    img: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        enum: ['active', 'inactive', 'out_of_stock']
    },
    brand: {
        type: String,
    },
    color: {
        type: String,
    },
    country_of_origin: {
        type: String,
    },
    size: {
        type: String,
    },
    quantity_per_box: {
        type: String,
    },
    product_code: {
        type: String,
    },
    expiration_date: {
        type: String,
    },
    description: {
        type: String,
    },
    creator: {
        type: String,
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: "Category"
    }

});

const Product = mongoose.model('Product', ProductSchema);
module.exports = Product;