const { validationResult } = require('express-validator');
const { body } = require('express-validator');
const Category = require('../../models/category')
const fs = require('fs');
const path = require('path');

module.exports = [


    body('name')
        .not().isEmpty().withMessage('The name is required.')
        .custom((value, { req }) => {
            return Category.find({ "name": value, "_id": { $ne: req.params.id } }).then(category => {
                if (category.length > 0) {
                    return Promise.reject('Category already exist.');
                }
            });
        }),

    (req, res, next) => {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {

            if (req.file) {

                fs.unlink(path.join('images', req.file.filename), (err) => {
                    if (err) {
                        return res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
                    }
                });
            }
           
            return res.status(400).json({ errors: errors.array() });
        }

        next();
    }
]