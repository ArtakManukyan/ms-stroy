const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

class Email {

    constructor({ data, mail }) {

        this.replacements = {
            mail,
            data,
        };

        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'artakmanukyan08@gmail.com',
                pass: 'seghandur76'
            }
        });

        this.readHTMLFile = function (path, callback) {

            fs.readFile(path, { encoding: 'utf-8' }, function (err, html) {

                if (err) {
                    throw err;
                    callback(err);
                }
                else {
                    callback(null, html);
                }
            });
        };
    }

    send() {

        const _this = this;

        const mailConfig = this.getHtml(this.replacements.mail);

        this.readHTMLFile(path.join(__dirname, '..', 'public/pages', mailConfig.html), function (err, html) {

            const template = handlebars.compile(html);

            const htmlToSend = template(_this.replacements);

            const mailOptions = {
                from: 'artakmanukyan08@gmail.com',
                to: 'artakmanukyan17@gmail.com',
                subject: mailConfig.subject,
                html: htmlToSend
            };

            _this.transporter.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                    callback(error);
                }
            });
        });
    }

    getHtml(mail){

        if(mail == 'contact'){

            return {
                html: 'emailContact.html',
                subject: 'Oбратный связь - ' + this.replacements.name,
            }

        } else if(mail == 'order'){
            return {
                html: 'emailOrder.html',
                subject: 'Новий заказ',
            }
        }
    }
}

module.exports = Email