const { validationResult } = require('express-validator');
const { body } = require('express-validator');

module.exports = [
    body('name')
        .not().isEmpty().withMessage('The name is required.'),
    body('email')
        .not().isEmpty().withMessage('The email is required.').isEmail().withMessage('Invalid email address.'),
    body('phone')
        .not().isEmpty().withMessage('The phone is required.'),
    body('address')
        .not().isEmpty().withMessage('The address is required.'),
    body('products')
        .isArray({ min: 1 }),




    (req, res, next) => {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {

            return res.status(400).json({ errors: errors.array() });
        }

        next();
    }
]