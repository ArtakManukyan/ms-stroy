const { validationResult } = require('express-validator');
const { body } = require('express-validator');
const fs = require('fs');
const path = require('path');

module.exports = [

    body('name').optional().isString(),
    body('price').optional().isString(),
    body('category').optional().isString(),
    body('status').optional().isString(),
    body('brand').optional().isString(),
    body('color').optional().isString(),
    body('country_of_origin').optional().isString(),
    body('size').optional().isString(),
    body('quantity_per_box').optional().isString(),
    body('product_code').optional().isString(),
    body('creator').optional().isString(),
    body('expiration_date').optional().isString(),
    body('description').optional().isString(),

    (req, res, next) => {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {

            if (req.file) {

                fs.unlink(path.join('images', req.file.filename), (err) => {
                    if (err) {
                        return res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
                    }
                });
            }

            return res.status(400).json({ errors: errors.array() });
        }

        next();
    }
]