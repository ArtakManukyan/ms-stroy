const {  validationResult } = require('express-validator');
const { body } = require('express-validator');

module.exports = [
    body('name')
    .not().isEmpty().withMessage('The name  is required.'),
    body('email')
    .not().isEmpty().withMessage('The email is required.').isEmail().withMessage('Invalid email address.'),
    body('phone')
    .not().isEmpty().withMessage('The phone is required.'),
    body('text')
    .not().isEmpty().withMessage('The text is required.'),
    

    (req, res, next) => {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {

            return res.status(400).json({ errors: errors.array() });
        }

        next();
    }
]