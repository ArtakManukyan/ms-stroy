const Product = require('../../models/product');
const fs = require('fs');
const path = require('path');

exports.all = async (req, res) => {

    try {
        const products = await Product.find({});

        res.json({ 'data': products, 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}
exports.create = async (req, res) => {

    const data = { ...req.body };
    data.img = req.file.filename;

    const product = new Product(data);

    try {

        await product.save();

        res.json({ 'data': product, 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}

exports.delete = async (req, res) => {

    try {

        const id = req.params.id;

        const product = await Product.findById(id);

        if (product){

            fs.unlink(path.join('images', product.img), (err) => {
                if (err){

                   return res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
                }
            });
        }

        await Product.findByIdAndDelete(id)

        res.json({ 'data': 'Product deleted successfully.', 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}

exports.update = async (req, res) => {

    try {

        const data = { ...req.body };

        const id = req.params.id;

        const product = await Product.findById(id);

        if (product && req.file){

            fs.unlink(path.join('images', product.img), (err) => {

                if (err){

                   return res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
                }
            });

            data.img = req.file.filename;
        }

        await Product.findByIdAndUpdate(id, data);

        res.json({ 'data': 'Category updated successfully.', 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}

exports.show = async (req, res) => {

    try {
        const id = req.params.id;

        const product = await Product.findById(id);

        res.json({ 'data': product, 'status': 'ok' });
    } catch (e) {

        res.status(400).json({ 'error': 'Oops, Something Went Wrong.', 'status': 'error' });
    }
}