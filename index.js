const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser')
const cors = require('cors');

const dbUrl = 'mongodb+srv://m001-student:test@sandbox.r8ahx.mongodb.net/stroy?retryWrites=true&w=majority';
const port = process.env.port || 8080;
const adminRoutes = require('./routes/admin');
const userRoutes = require('./routes/user');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/images', express.static('images'))

app.use('/admin', adminRoutes);
app.use('/user', userRoutes);

const start = async () => {

    await mongoose.connect(dbUrl,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        });
       

    app.listen(port, () => {

        console.log(`Started on port - ${port}`);
    })
}

start();